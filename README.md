# Colophon, or a ReadMe #

The personal portfolio site of Will Guldin. Originally published 2011ish. Iterative updates still ongoing.

------------------------------------------

The typeface is Texta, designed by Daniel Hernández Sánchez & Miguel Hernández Montoya ©2014.
Purchased through Fontspring.

Built with SCSS, HTML5, Javascript, and a little PHP. 
* Comments give full credit to any code snippets or plugins used. Open Source FTW!

Hosting provided by Digital Ocean.

------------------------------------------

Thanks for looking around,

Will