<footer class="site-footer">
    <?php /* <nav role="navigation">
        <ul>
            <li><a class="link- -highlight" href="#recent-work">Work</a></li>
            <li><a class="link- -highlight" href="#contact">Contact</a></li>
            <li><a class="link- -highlight" href="#">Blog</a></li>
        </ul>
    </nav> */ ?> 
    <ul class="footer-contact">
        <li>&copy; Will Guldin</li>
        <li><a class="link--highlight" href="tel:1-501-920-7064">501.920.7064</a></li>
        <li><span class="email-link">wguldin [at] gmail [dot] com</span></li>
    </ul>
</footer>
    <script src="/js/plugins.js?v=15"></script>
    <script src="/js/script.js?v=15"></script>
</body>
</html>