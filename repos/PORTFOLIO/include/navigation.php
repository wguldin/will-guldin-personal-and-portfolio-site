<nav role="navigation">
    <a class="nav-logo-link" href="/"><img class="nav-logo" src="/img/mini-logo.jpg" alt="Will Guldin Design"/></a>
    <ul class="nav-list">
        <li class="nav-list-item"><a class="link--highlight" href="/#main">Work</a></li>
        <li class="nav-list-item"><a class="link--highlight" href="/#contact">Contact</a></li>
        <li class="nav-list-item"><a class="link--highlight" href="http://blog.willguldin.com">Blog</a></li>
    </ul>

    <span class="nav-icon--mobile"><span class="nav-icon__text">Show Menu</span></span>
</nav>  