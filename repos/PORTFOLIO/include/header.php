<!DOCTYPE html>
<!--[if IE 7]><html lang="en" class="no-js ie lt-ie10 lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie lt-ie10 lt-ie9"><![endif]-->
<!--[if IE 9]><html lang="en" class="no-js ie lt-ie10"><![endif]-->
<!--[if (gt IE 9)|(IE)]><html lang="en" class="no-js ie"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="en"><!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?= isset($PageTitle) ? $PageTitle : "Will Guldin, Designer and Front-End Developer in Middleton, Wisconsin"?></title>
    <meta name="description" content="Will Guldin is a digital designer and consultant out of Columbia, Missouri, with several years of experience. He is currently looking for a full-time digital design position.">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <?php if (function_exists('customPageHeader')){
      customPageHeader();
    }?>
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.5.6/webfont.js"></script>
    <script>
      WebFont.load({
        custom: {
          families: ['texta: 3n, 4n, 4i, 5n, 8n']
        }
      });
    </script>

    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">

    <!--[if !lt IE 9]><!-->
        <link rel="stylesheet" type="text/css" href="/css/style.css?v=12" />
    <!--<![endif]-->

    <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" href="/css/style_ie.css?v=12" />
            <script src="/js/shiv.min.js"></script>
    <![endif]-->
</head>