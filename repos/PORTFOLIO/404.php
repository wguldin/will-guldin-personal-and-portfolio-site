<?php
    include_once("include/header.php");
?>

<body class="fit-page">

<?php 
    include_once("include/navigation.php");
?>

<main role="main" class="full-width error-page">
  <header class="intro" role="banner">
    
    <div class="intro-main-svg-frame is-404">
        <img class="logo-404" src="/img/logo-broken.svg" alt="404 page logo">
    </div>

    <h1 class="intro__main-head">404!</h1>
    
    <div class="intro__text">
        <p class="intro__description">We made a mistake and couldn't find what you were looking for. Sorry about that! Trying going back to the homepage.</p>
        <a href="/" class="chiclet-button chiclet-button--primary intro__button">Return to the homepage</a>
    </div>
</header>

</main>

</body>