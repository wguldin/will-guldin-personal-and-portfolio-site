<?php
    include_once($_SERVER['DOCUMENT_ROOT']."/include/header.php");
?>

<body>

<?php 
    include_once($_SERVER['DOCUMENT_ROOT']."/include/navigation.php");
?>

<header class="intro case-study__header" role="banner">
    <div class="intro__text--wrapper">
        <h1 class="header__intro" id="HundredAgesHeading">
            <span class="intro__main-head"><span id="headlineNumber">100</span><span> Ages</span></span>
            <span class="intro__subheader">A Century of Voices</span>
        </h1>
        <h2 class="intro__description" id="HundredAgesDesc">A pair of photojournalists recorded and edited 100 interviews with people of all ages. With my help, they told a timeless story about life's experiences.</h2>
    </div>
    <span class="age age--1">37</span>
    <span class="age age--2">23</span>
    <span class="age age--3">4</span>
    <span class="age age--4">78</span>
    <span class="age age--5">56</span>
    <span class="age age--6">89</span>
    <span class="age age--7">17</span>
    <span class="age age--8">64</span>
    <span class="age age--9">95</span>
    
</header>

<main role="main" id="main">
<article aria-labelledby="HundredAgesHeading" aria-describedby="HundredAgesDesc" >
    <section>
        <p>When Miguel Rodriguez, 52, arrived at the hospital after blacking out, he thought to himself “You might have one chance in life and that was my last chance.”</p>

        <p>That was more than 17 years ago, and he’s been sober ever since.</p>
        
        <figure class="article__image">
            <img src="/img/052_400.jpg" alt="Miguel Rodriguez, 52 years old" />
            
            <figcaption class="article__quote">
                <blockquote>
                    <p>“You might have one chance in life and that was my last chance."</p>
                    <cite>&mdash; Miguel Rodriguez, 52</cite>
                </blockquote>
            </figcaption>    
        </figure>

        <p>His story is one that we all can relate to in some way — either through our own struggles or though those of people we know. That’s the power of 100 Ages, A Century of Voices. Each interview with someone of a different age tells you a little bit more about the experiences we all share.</p>

        <p>That’s what excited me about the project, and why I wanted to be a part of it. Since it had such strong storytelling behind it, my ultimate goal was to create a design that would stay out of the way.</p>
 
        <h3>About the Columbia Missourian</h3>

        <p>The organization behind this project was the <a href="http://www.columbiamissourian.com/" class="link--highlight">Columbia Missourian</a>, a 24/7 news operation that covers Columbia, Mo. It is part of the Missouri School of Journalism and uses professional journalists as its editors and teachers. The paper routinely wins recognition for its reporting.</p>

        <aside class="infobox">
            <h4 class="infobox--header">Project Details</h4>
            <dl>
                <dt class="infobox--subheader">Timeline</dt>
                <dd>Late Spring - Summer 2013</dd>
                <dt class="infobox--subheader">Responsibilities</dt>
                <dd>
                    <ul>
                        <li>Worked with editors &amp; photojournalists to create the project's design.</li>
                        <li>Coded all parts of website and used a frontend framework to build the site.</li>                                                             
                    </ul>
                </dd>
                <dt class="infobox--subheader">Columbia Missourian traffic</dt>
                <dd>+250,000 unique visitors/month</dd>
                <dt class="infobox--subheader">Cutest interview</dt>
                <dd><a href="http://columbiamissourian.com/app/legacy/100ages/#/2" class="link--highlight">2-year-olds Adam &amp; Max Sedillo</a></dd>
            </dl>
        </aside>

        <p>After agreeing to work on the site for the project, I met with the editors of the Missourian and with the two photojournalists leading the project. We discussed what the goals for the project would be and all agreed that the final product should stand out from the Missourian’s regular website and reflect the quality of the paper’s journalism.</p> 

        <p>These kinds of early meetings are often the most important part of my design process and guide the work that comes next.</p>

        <h3>Initial designs</h3>

        <p>After discussing the project and its goals, I took a few days to come up with an early design draft.</p>

        <p>I quickly settled on using large portrait photos of each person being interviewed, along with a modern typeface to give the project a clean look. This was all inspired by a simple design style that puts the focus on the content.</p>

        <p>By making these few layouts I was able to quickly make sure the Missourian and I were on the same page. With their signoff, I began to build the website. Working in the evenings after my full-time design internship at the Reading Eagle, I was able to finish the project in two months.</p>

        <figure class="device__frame responsive-image">
            <img src="/img/hundred-ages-screenshot.jpg" class="device__image" alt="100 Ages, A Century of Voices website screenshot shown on a smart phone" />
            <figcaption class="device__caption">
                100 Ages, and every project I work on, is designed to work well on a range of devices, including smartphones. 
            </figcaption> 
        </figure>

        <h3>Responsive to a mobile web</h3>

        <p>Touchscreen devices are poised to outpace desktop usage online, if they haven’t done so already. Every site I’ve built is “responsive” meaning the same website adapts its design to each device viewing it, from a cramped smartphone to an oversized monitor.</p>

        <p>For 100 Ages, I took this a step farther, ensuring all the photographs used in the project were large enough to look sharp on retina high-definition screens. On displays that weren’t as crisp, a smaller image loaded instead. So, visitors only had to download a picture as large as they needed for their device.</p>

        <p>I also made use of a tool (AngularJS) that allowed visitors to go from interview to interview without having to refresh their browser. On slow mobile connections, this made for a faster experience. It also helped accomplish our goal of making the site resemble the feeling of a native app.</p>

        <h3>A bold experiment</h3>

        <p>100 Ages, A Century of Voices, reflected a kind of web project that was difficult to undertake, and it was inspiring to be a part of it. Within days of publication the project recieved attention on several photography blogs and ultimately won an Award of Excellence from the <a class="link--highlight" href="http://www.snd.org/">Society for News Design.</a></p>

        <p>All that would be moot, however, if I wasn’t satisfied with the final product. Like many who visited the site, though, I came away with a sense that the project told us all a little bit more about what it is to be human.</p>

        <p>Here’s what one of the photojournalists on the project had to say about my involvement:</p>

        <blockquote class="article__quote--featured"> 
            <p>&ldquo;Will Guldin is that rare combination of both hard work and talent.  Will went above and beyond the call of duty when he served as the designer for the 100 Ages: A Century of Voices ​​project, sharing our vision of a clean and sophisticated layout.  He was always ready to hear our thoughts, while contributing his own expertise to make the final product flawless in multiple formats.&rdquo;</p>
            <cite><a class="link--highlight" href="http://katiealaimo.virb.com/">Photojournalist Kathleen Alaimo</a></cite>
        </blockquote>

        <p>I hope you’ll take a look at the work as well. I’ve learned a lot more since I first completed this project, but it remains one of the most valuable things I’ve done in my professional career.</p>

        <a href="http://columbiamissourian.com/app/legacy/100ages/" class="chiclet-button">Visit the site</a>

    </section> 
</article>
</main>

<?php
    include_once($_SERVER['DOCUMENT_ROOT']."/include/footer.php");
?>
