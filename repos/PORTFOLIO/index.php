<?php include_once("include/header.php"); ?>

<body>

<?php include_once("include/navigation.php"); ?>

<header class="intro" role="banner">
    <div class="intro__wrapper" itemscope itemtype="http://schema.org/Person">
        <div class="intro-main-svg-frame is-logo" itemscope itemtype="http://schema.org/Organization">
            <meta itemprop="name" content="Will Guldin Design" /> 
            <span class="intro-main-svg-icon" data-icon-name="logo">
                <img itemprop="logo" src="/img/logo-fallback.png" alt="Will Guldin Design logo">
            </span>
        </div>
        <div class="intro__text--wrapper">
          <div class="intro__header"> 
              <h2 class="intro__subheader">Designer &amp; Front-End Developer</h2>
              <h1 class="intro__main-head" itemprop="name">Will Guldin</h1>
          </div> 
   
          <aside class="intro__text">
              <a href="#contact" class="chiclet-button chiclet-button--primary intro__button">Contact me with any questions</a>
              <p class="intro__description" itemprop="description">After working for <span itemprop="worksFor">an eCommerce company</span> for the past two years, my wife and I just moved. Now, I'm looking for a full-time design or front-end development position. To get in touch, email me at <span class="email-link">wguldin [at] gmail [dot] com</span>, or call <a href="tel:1-501-920-7064" class="link--highlight">501.920.7064.</a></p>
          </aside>
        </div>
        <meta itemprop="telephone" content="501-920-7064" />
        <meta itemprop="email" content="wguldin@gmail.com" />
        <meta itemprop="url" content="http://willguldin.com" />
    </div>
    <span class="refresh-trigger"></span>
</header>

<main role="main" id="main">
    <section class="portfolio portfolio--case-study checkout" itemscope itemtype="http://schema.org/WebSite">
      <h2 class="section-header">A Faster, Smarter Checkout</h2>
      <figure class="portfolio--case-study--description">
          <div class="checkout__image"><img itemprop="image" src="/img/checkout-mobile-screenshot.jpg" alt="Screenshot from checkout redesign of shipping address selection page"></div>
          <figcaption itemprop="description">
              <p><strong class="section-text-summary">After joining a large eCommerce company, I helped create a redesigned checkout process that was quicker and simpler for our customers.</strong></p>
              <p>As a lead designer on the project, I helped create user flows, mockups and full-fledged designs, which we then user tested and tweaked.</p>
              <a href="/checkout-redesign" class="chiclet-button">Read the case study</a>            
          </figcaption>
      </figure>
    </section>
    <section class="jack portfolio example" itemscope itemtype="http://schema.org/Periodical">
        <h2 class="section-header" itemprop="name">JACK. Magazine</h2>
        
        <meta itemprop="issueNumber" content="1" />
        <meta itemprop="datePublished" content="2013-07-04" />   
        <meta itemprop="author" content="Will Guldin, Jamie Hausman, Kaylen Ralph, Anna Bolka, Rikki Byrd, Michael Pottebaum"/>
        <meta itemprop="editor" content="John Fennell, Erica Mendez Babcock" />
        <meta itemprop="award" content="1st place award from Association for Education in Journalism and Mass Communication in Start-Up Magazine Project Category"/>
        <meta itemprop="keywords" content="JACK. Magazine, Cooking, Drinking, Entertaining, Recipes, Restaurants, Meredith Corporation, University of Missouri, Publication Design, Mag+, iPad app"/>
        <meta itemprop="genre" content="Cooking, Drinking, Entertaining"/>
        <meta itemprop="audience" content="Young professional men"/>

        <meta itemprop="sourceOrganization" content="Meredith Corporation"/>
        <meta itemprop="publisher" content="Meredith Corporation"/>
        <meta itemprop="publishingPrinciples" content="http://www.meredith.com/meredith_corporate/mission_statement.html"/>

        <div class="section-content">
            <div class="example-slider lazyload-container">
              <ul class="image--main__slider">
                <li class="is-fallback"><figure><img class="image--main" src="/img/jack/jack_01.jpg" alt="Prototype layout from Jack Magazine, for a story about Bell's Brewery." /><figcaption class="caption">To capture this image, we worked with a photographer to set up a studio shoot.</figcaption></figure></li>
                <li><figure><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/jack_02.jpg" alt="Round up of some of the best BBQ places in the country." /><figcaption class="caption">Instagram photos provided the art for this BBQ-joint roundup.</figcaption></figure></li>
                <li><figure><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/jack_03.jpg" alt="Write up of BBQ joint Lillie Q's in Chicago, IL." /><figcaption class="caption">An inside page from the barbeque roundup.</figcaption></figure></li>
                <li><figure><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/jack_04.jpg" alt="Hop illustration for article about the beer Hopslam" /><figcaption class="caption">For this story about Bell Brewery's Hopslam, I reached out to a pair of hop-inspired artists.</figcaption></figure></li>
                <li><figure><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/jack_05.jpg" alt="Inside page from the Hopslam story" /><figcaption class="caption">Cara &amp; Louie Beryl's work was a great compliment for this story.</figcaption></figure></li>
                <li><figure><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/jack_06.jpg" alt="Pierce Courchaine steps up to cook a big meal" /><figcaption class="caption">We setup another photo shoot for this piece, about our writer cooking a date-night meal.</figcaption></figure></li>
                <li><figure><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/jack_07.jpg" alt="Jack's recipe index" /><figcaption class="caption">Jack, a prototype, was pitched as a tablet app, with a website and quarterly print cookbook.</figcaption></figure></li>
                <li><figure><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/jack_08.jpg" alt="Cooking salmon on a plank" /><figcaption class="caption">Meredith, publisher of Better Homes &amp; Gardens, gave us access to its image library.</figcaption></figure></li>                 
              </ul>
              <ul class="image--nav__slider">
                <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/nav/jack_01.jpg" alt="Prototype layout from Jack Magazine, for a story about Bell's Brewery." /></li>
                <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/nav/jack_02.jpg" alt="Round up of some of the best BBQ places in the country." /></li>
                <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/nav/jack_03.jpg" alt="Write up of BBQ joint Lillie Q's in Chicago, IL." /></li>
                <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/nav/jack_04.jpg" alt="Hop illustration for article about the beer Hopslam" /></li>
                <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/nav/jack_05.jpg" alt="Inside page from the Hopslam story" /></li>
                <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/nav/jack_06.jpg" alt="Pierce Courchaine steps up to cook a big meal" /></li>
                <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/nav/jack_07.jpg" alt="Jack's recipe index" /></li>
                <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/jack/nav/jack_08.jpg" alt="Cooking salmon on a plank" /></li>                
              </ul>
            </div>            
            <div class="section-text"> 
                <p itemprop="description">
                    <strong class="section-text-summary">Meredith Corporation, a large magazine publisher, was looking for a different take on the traditional food magazine.</strong>
                </p>
                <p>To meet this challenge, we came up with JACK., a prototype cooking publication targeted toward young men. As Creative Director, I led a team of designers as we refined the brand and visual voice for JACK, which was primarily designed as a tablet app.</p>
                <p>I presented our design work to Meredith executives, who were impressed. The project, which was done while I was attending the Missouri School of Journalism, later won first place in a national student publication contest.</p>
            </div>
        </div>
    </section>
    <section class="portfolio portfolio--case-study insert ssnd" itemscope itemtype="http://schema.org/WebSite">
      <h2 class="insert-header">Helping a contest live up to its reputation</h2>
      <figure> 
          <div class="section-main-image">
            <a class="ssnd-link" href="http://ssnd.missouri.edu">           
              <img itemprop="image" src="/img/ssnd_logo.jpg" alt="Student Society for News Design logo">
              <span>View the redesigned website</span>
            </a> 
          </div>
          <figcaption class="insert-description" itemprop="description">
              <p>The annual Student Society for News Design contest draws in hundreds of entries. This work represents the best student design in the country.</p>
              <p>SSND's website didn't meet this standard. So, I redesigned it to make it easier to enter the contest and gave it a more modern aesthetic. Setting up a CMS for the organization and migrating their blog from a separate website helped to better focus their online presence while making it easier to maintain.</p>
          </figcaption>
      </figure>
    </section>
    <section class="portfolio graphic example">
      <h2 class="section-header">Graphic Design Experience</h2>
        <div class="section-content">
          <div class="example-slider lazyload-container">
            <ul class="image--main__slider variable-height">
              <li class="is-fallback"><figure><div class="variable-height__container"><div><img class="image--main" src="/img/graphic-design/print_01.jpg" alt="Proposed cover for Vox Magazine" /></div></div><figcaption class="caption">To create the aesthetic I was looking for, I hand drew these cables. This illustration was the main art for the inside story.</figcaption></figure></li>
              <li><figure><div class="variable-height__container"><div><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/print_02.jpg" alt="Columbia Missourian sports front" /></div></div><figcaption class="caption">This was the last scheduled game between MU and rival Kansas. It was close, and this page had to be designed on a tight deadline.</figcaption></figure></li>
              <li><figure><div class="variable-height__container"><div><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/print_03.jpg" alt="Columbia Missourian infographics two-page spread" /></div></div><figcaption class="caption">As an infographics reporter, I both researched and designed this graphic on election spending. I also created a digital version using d3.js. This infographic won 2nd place in Missouri.</figcaption></figure></li>
              <li><figure><div class="variable-height__container"><div><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/print_04.jpg" alt="Cover for Vox Magazine" /></div></div><figcaption class="caption">A family fled Iraq for the US after receiving death threats from the Taliban. I recreated one of those threats  bullets in an envelope.</figcaption></figure></li>
              <li><figure><div class="variable-height__container"><div><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/print_05.jpg" alt="Vox Magazine Inside Feature Design" /></div></div><figcaption class="caption">The only good photo we had of this local basketball legend, was a vertical shot. But Vox is printed on square paper. So, I put the layout on its side, to give the image the room it deserved, and make a poster-style tribute to Cecil Estes.</figcaption></figure></li>
              <li><figure><div class="variable-height__container"><div><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/print_06.jpg" alt="Proposed cover for Vox Magazine" /></div></div><figcaption class="caption">An unpublished Vox Cover for a story about night-shift workers. I designed the typeface for the cover myself.</figcaption></figure></li>
              <li><figure><div class="variable-height__container"><div><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/print_07.jpg" alt="Reading Eagle local section front" /></div></div><figcaption class="caption">For these polling results, I wanted to display as much information as possible in a readable layout.</figcaption></figure></li>
              <li><figure><div class="variable-height__container"><div><img class="image--main" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/print_08.jpg" alt="Illustration for Vox Magazine" /></div></div><figcaption class="caption">A fun illustration for a story about a basketball tournament MU was playing in. Vox, a city magazine, often has a lighter tone than other publications.</figcaption></figure></li>                  
            </ul>
            <ul class="image--nav__slider">
              <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/nav/print_01.jpg" alt="Proposed cover for Vox Magazine" /></li>
              <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/nav/print_02.jpg" alt="Columbia Missourian sports front" /></li>
              <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/nav/print_03.jpg" alt="Columbia Missourian infographics two-page spread" /></li>
              <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/nav/print_04.jpg" alt="Cover for Vox Magazine" /></li>
              <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/nav/print_05.jpg" alt="Vox Magazine Inside Feature Design" /></li>
              <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/nav/print_06.jpg" alt="Proposed cover for Vox Magazine" /></li>
              <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/nav/print_07.jpg" alt="Reading Eagle local section front" /></li>
              <li><img class="image--nav" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy="/img/graphic-design/nav/print_08.jpg" alt="Illustration for Vox Magazine" /></li>                
            </ul>
          </div>
          <div class="section-text"> 
            <p>
                <strong class="section-text-summary">Although most of my work lately has been focused on digital design and development, I also have experience as a graphic designer.</strong>
            </p>
            <p>This given has me a solid foundation in the fundamentals of good design, and it's something I draw on in all of my digital design work. My experience with graphic design comes mostly from magazines and newspapers. This work includes conceptual illustrations, page layouts, logos and infographics.</p>
            <p>As a designer, I believe our work should always try to solve a problem. But the best design happens when the solution to that problem is also beautiful, both in its aesthetic, and in its simplicity. I try to meet that standard in all that I do.</p>
          </div>
        </div>  
    </section>
    <section class="portfolio portfolio--case-study ages" itemscope itemtype="http://schema.org/WebSite">
      <meta itemprop="url" content="http://media.columbiamissourian.com/100ages/#/"/>
      <meta itemprop="datePublished" content="2013-07-04"/>   
      <meta itemprop="author" content="Will Guldin, Katie Alaimo, Alyssa Goodman"/>
      <meta itemprop="editor" content="Laura Johnston"/>
      <meta itemprop="award" content="2013 Award of Excellence from the Society for News Design"/>
      <meta itemprop="isFamilyFriendly" content="true"/>
      <meta itemprop="keywords" content="100 Ages, Century of Voices, photojournalism, Columbia Missourian, Boone County, Columbia, Angular JS"/>
      <meta itemprop="genre" content="Journalism"/>

      <meta itemprop="sourceOrganization" content="Columbia Missourian"/>
      <meta itemprop="publisher" content="Columbia Missourian"/>
      <meta itemprop="publishingPrinciples" content="http://www.columbiamissourian.com/about/"/>

      <h2 class="section-header" itemprop="name">100 Ages, A Century of Voices</h2>
      <figure class="section-content portfolio--case-study--description">
          <div class="portfolio-image section-main-image lazyload-container">
              <picture> 
                  <!--[if IE 9]><video style="display: none;"><![endif]-->
                      <source media="(min-width: 90em)" data-srcset="/img/ages-large.jpg, /img/ages-extralarge.jpg 2x" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==">
                      <source media="(min-width: 70em)" data-srcset="/img/ages-medium.jpg, /img/ages-large.jpg 2x" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==">
                      <source media="(min-width: 52em)" data-srcset="/img/ages-small.jpg, /img/ages-medium.jpg 2x" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==">
                      <source data-srcset="/img/ages-medium.jpg, /img/ages-large.jpg 2x" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==">
                  <!--[if IE 9]></video><![endif]-->
                  <!--[if gte IE 9]><!-->
                    <img class="lazyload" itemprop="image" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="100 Ages, A Century of Voices">
                  <!--<![endif]-->
                  <!--[if lt IE 9]><img class="lazyload" itemprop="image" src="/img/ages-small.jpg" alt="100 Ages, A Century of Voices"><![endif]-->
              </picture>
          </div>
          <figcaption itemprop="description">
              <p><strong class="section-text-summary">This collection of personal stories underscores the common bonds of life that we all share.</strong></p>
              <p>As the only designer for this project, I came up with an effective and simple presentation for it, while coding all parts of the website.</p>
              <a href="/one-hundred-ages" data-pjax class="chiclet-button">Read the case study</a>            
          </figcaption>
      </figure>
    </section>
    
    <section class="portfolio portfolio--case-study rdes" itemscope itemtype="http://schema.org/WebSite">
        <meta itemprop="url" content="http://rdes.willguldin.com"/>
        <meta itemprop="author" content="Will Guldin"/>
        <meta itemprop="isFamilyFriendly" content="true"/>
        <meta itemprop="keywords" content="Roy Dudley Estate Sales, Little Rock, Responsive, Perch CMS"/>
        <meta itemprop="genre" content="Estate Sales"/>

        <meta itemprop="sourceOrganization" content="Roy Dudley Estate Sales"/>
        <meta itemprop="publisher" content="Roy Dudley Estate Sales"/>
        <meta itemprop="publishingPrinciples" content="http://www.roydudleyestatesales.com/rdsalesstaff.html"/>

        <h2 class="section-header" itemprop="name">Roy Dudley Estate Sales</h2>
        <figure class="section-content portfolio--case-study--description">
            <figcaption itemprop="description">
              <p><strong class="section-text-summary">Roy Dudley Estate Sales needed a website that could highlight upcoming sales and be easy to edit and maintain.</strong></p>
              <p>Working with them, I came up with a design that puts more focus on the company's upcoming sales and that makes it simple to sign up for their email list.</p>
              <p>I also made sure the site could be edited without any downtime, something that wasn't possible before.</p>
            </figcaption>   
            <div class="portfolio-image section-main-image lazyload-container">
              <picture>
                  <!--[if IE 9]><video style="display: none;"><![endif]-->
                      <source media="(min-width: 90em)" data-srcset="/img/rdes-large.jpg, /img/rdes-extralarge.jpg 2x" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="> 
                      <source data-srcset="/img/rdes-medium.jpg, /img/rdes-large.jpg 2x" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==">
                  <!--[if IE 9]></video><![endif]-->
                  <!--[if gte IE 9]><!-->
                  <img class="lazyload" itemprop="image" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="  alt="Roy Dudley Estate Sales Test Website">
                  <!--<![endif]-->
                  <!--[if lt IE 9]><img class="lazyload" itemprop="image" src="/img/rdes-large.jpg" alt="Roy Dudley Estate Sales Test Website"><![endif]-->
              </picture>
            </div>
        </figure>
    </section>

    <section id="contact" class="contact">
        <h2 class="section-header">How can I help you?</h2>
        <div class="section-content">
            <p>If you have any questions about my work, or would like to talk about a project, just drop a line here using this form. You can also email me at <span class="email-link">wguldin [at] gmail [dot] com</span>.</p>
            <div class="contact-form__container has-loader">
                <?php include("include/form.php"); ?>
            </div>
            <div class="about">
              <div class="lazyload-container about__mug">
                <picture>
                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                        <source media="(min-width: 80em)" data-srcset="/img/portrait-large.jpg, /img/portrait-extralarge.jpg 2x" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==">
                        <source data-srcset="/img/portrait-medium.jpg, /img/portrait-large.jpg 2x" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==">
                    <!--[if IE 9]></video><![endif]-->
                    <!--[if gte IE 9]><!-->
                    <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="  alt="Portrait of Will Guldin, Digital Designer">
                    <!--<![endif]-->
                   <!--[if lt IE 9]><img class="lazyload" src="/img/portrait-medium.jpg"  alt="Portrait of Will Guldin, Digital Designer"><![endif]-->
                </picture>
              </div>

              <h3>About Me</h3>
              <div class="description">
                  <p>My interest with visual design started in high school when I practiced film art photography. I studied journalism in college and eventually discovered that digital design was the perfect mixture of my technical and creative skills.</p>
                  <p>In my spare time, you might find me outside biking with my wife, Megan, or cooking in the kitchen.</p>
              </div>
           </div>
        </div>
    </section>
</main>

<?php include_once("include/footer.php"); ?>